import 'dart:io';

import 'package:fltest/widgets/appString.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  File? filePath;
  String videoPath = "";
  XFile? filetobepicked;
  File? getFile;
  var getfiless;
  TextEditingController getvideotrimTrimText = TextEditingController();

  Future videopicked() async {
    final picker = ImagePicker();
    filetobepicked = await picker.pickVideo(
        source: ImageSource.gallery, maxDuration: Duration(seconds: 100));
    if (filetobepicked == null) {
      Fluttertoast.showToast(msg: "Can't Find the Path");
    } else {
      setState(() {
        videoPath = filetobepicked!.path.toString();
      });
      print(filetobepicked.toString());
    }
  }

  Future videoTrim(int duration) async {
    try {
      if (filetobepicked != null) {
        VideoPlayerController videoLengthController =
            VideoPlayerController.file(File(filetobepicked!.path));

        await videoLengthController.initialize();

        if (videoLengthController.value.duration.inSeconds == 100 &&
            duration == 10) {
          for (int i = 0; i <= duration; i++) {
            if (videoLengthController.value.duration.inSeconds > 0) {
              filetobepicked = null;
              setState(() {
                getfiless = videoLengthController.value.duration.inSeconds * i;
              });
            }
          }
        } else {
          throw ('you video is short');
        }
        // testLengthController.dispose();
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppString.text),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 28.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextWidget(videoPath),
              const SizedBox(
                height: 10,
              ),
              InkWell(
                  onTap: () {
                    videopicked();
                  },
                  child: buttonWidget()),
              const SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: getvideotrimTrimText,
                decoration:
                    const InputDecoration(hintText: "Please Enter Input"),
              ),
              const SizedBox(
                height: 10,
              ),
              InkWell(
                  onTap: () {
                    videoTrim(int.parse(getvideotrimTrimText.text));
                  },
                  child: buttonWidget()),
              const SizedBox(
                height: 10,
              ),
              TextWidget(getfiless.toString()),
            ],
          ),
        ),
      ),
    );
  }

  Widget buttonWidget() {
    return Container(
      height: 30,
      width: 50,
      color: Colors.red,
      child: Center(child: Text("Click")),
    );
  }

  Widget TextWidget(String pathget) {
    return pathget == null || pathget == ""
        ? Container()
        : Row(
            children: [
              const Text("File Path: "),
              Expanded(
                child: Text(
                  pathget,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          );
  }
}
